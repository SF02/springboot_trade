package citi.com.training.trade.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import citi.com.training.trade.model.TradeRequest;
import citi.com.training.trade.model.TradeType;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TradeControllerTests {
    
    @Autowired
    private TestRestTemplate restTemplate;

    @Disabled
    @Test
    public void test_save(){
        TradeRequest testTradeRequest = new TradeRequest();
        testTradeRequest.setRequestedPrice(20.00);
        testTradeRequest.setStockQuantity(2);
        testTradeRequest.setStockTicker("Stock");
        testTradeRequest.setTradeType(TradeType.BUY);

        ResponseEntity<TradeRequest> response = restTemplate.postForEntity("/v1/trade", 
                                                testTradeRequest, TradeRequest.class);

        assertEquals(response.getStatusCode(), HttpStatus.CREATED);

    }
}
