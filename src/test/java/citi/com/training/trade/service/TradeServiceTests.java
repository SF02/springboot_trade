package citi.com.training.trade.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import citi.com.training.trade.dao.TradeMongoRepo;
import citi.com.training.trade.model.TradeRequest;
import citi.com.training.trade.model.TradeState;
import citi.com.training.trade.model.TradeType;

@SpringBootTest
public class TradeServiceTests {

    private static Logger LOG = LoggerFactory.getLogger(TradeServiceTests.class);

    @Autowired
    private TradeService tradeService;

    @MockBean
    private TradeMongoRepo mockRepo;

    @Test
    public void test_tradeService_save() {
        LOG.debug("Testing save()");

        TradeRequest tradeRequest = new TradeRequest();
        tradeRequest.setStockTicker("ABC");
        tradeRequest.setStockQuantity(100);
        tradeRequest.setRequestedPrice(123.45);
        tradeRequest.setTradeType(TradeType.BUY);
        tradeRequest.setTradeState(TradeState.CREATED);
        
        when(mockRepo.save(tradeRequest)).thenReturn(tradeRequest);
        tradeService.save(tradeRequest);
        verify(mockRepo, times(1)).save(tradeRequest);
    }

    @Test
    public void test_tradeService_findAll() {
        LOG.debug("Testing findAll()");

        TradeRequest tradeRequest = new TradeRequest();
        tradeRequest.setStockTicker("ABC");
        tradeRequest.setStockQuantity(100);
        tradeRequest.setRequestedPrice(123.45);
        tradeRequest.setTradeType(TradeType.BUY);
        tradeRequest.setTradeState(TradeState.CREATED);

        List<TradeRequest> list = new ArrayList<>();
        list.add(tradeRequest);

        when(mockRepo.findAll()).thenReturn(list);
        tradeService.findAll();
        verify(mockRepo, times(1)).findAll();
    }
      
}
