package citi.com.training.trade.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TradeRequestTests {
    
    @Test
    public void testConstructorWithArgs() {
        TradeRequest testTradeRequest = new TradeRequest();
        testTradeRequest.setStockTicker("GOOGL");
        testTradeRequest.setStockQuantity(5);
        testTradeRequest.setRequestedPrice(1500.00);
        testTradeRequest.setTradeType(TradeType.BUY);

        TradeRequest testTradeRequestConstructor = new TradeRequest("GOOGL", 5, 1500.00, TradeType.BUY);
        System.out.println(testTradeRequestConstructor.toString());
        //assertEquals(testTradeRequest, testTradeRequestConstructor);
    }

    @Test
    public void testId() {
        TradeRequest testTradeRequest = new TradeRequest();
        testTradeRequest.setId("1234");
        assertEquals(testTradeRequest.getId(), "1234");
    }

    @Test
    public void testDateTime() {
        TradeRequest testTradeRequest = new TradeRequest();
        System.out.println((testTradeRequest.getDateTimeCreated()));
    }

    @Test
    public void testStockTicker() {
        TradeRequest testTradeRequest = new TradeRequest();
        testTradeRequest.setStockTicker("GOOGL");
        assertEquals(testTradeRequest.getStockTicker(), "GOOGL");
    }

    @Test
    public void testStockQuantity() {
        TradeRequest testTradeRequest = new TradeRequest();
        testTradeRequest.setStockQuantity(5);
        assertEquals(testTradeRequest.getStockQuantity(), 5);
    }

    @Test
    public void testRequestedPrice() {
        TradeRequest testTradeRequest = new TradeRequest();
        testTradeRequest.setRequestedPrice(1500.00);
        assertEquals(testTradeRequest.getRequestedPrice(), 1500.00);
    }

    @Test
    public void testTradeType() {
        TradeRequest testTradeRequest = new TradeRequest();
        testTradeRequest.setTradeType(TradeType.BUY);
        assertEquals(testTradeRequest.getTradeType(), TradeType.BUY);
    }

    @Test
    public void testTradeState() {
        TradeRequest testTradeRequest = new TradeRequest();
        testTradeRequest.setTradeState(TradeState.CREATED);
        assertEquals(testTradeRequest.getTradeState(), TradeState.CREATED);
    }
    
}
