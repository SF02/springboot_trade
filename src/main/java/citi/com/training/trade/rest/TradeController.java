package citi.com.training.trade.rest;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import citi.com.training.trade.model.TradeRequest;
import citi.com.training.trade.service.TradeService;

@CrossOrigin("*")
@RestController
@RequestMapping("/v1/trade")
public class TradeController {
    
    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
    
    @Autowired
    private TradeService tradeService;


    @RequestMapping(method = RequestMethod.GET)
    public List<TradeRequest> findAll() {
        LOG.debug("findAll() request recieved");

        return tradeService.findAll();
    }

    // doesnt run when this isnt commented out
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<TradeRequest> findOne(@PathVariable String id) {
        LOG.debug("findOne() request recieved: " + id);
        
        return tradeService.findOne(id);
    }


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<TradeRequest> save(@RequestBody TradeRequest tradeRequest){
        LOG.debug("save() trade request recieved");

        return new ResponseEntity<TradeRequest>(tradeService.save(tradeRequest),
                                                HttpStatus.CREATED);
    } 

    //V2
    // @RequestMapping(method = RequestMethod.PUT)
    // public ResponseEntity<TradeRequest> updateQuantity(@PathVariable String id, @PathVariable int quanitity) {
    //     LOG.debug("update() request received: " + id);
        
    //     return new ResponseEntity<TradeRequest>(tradeService.updateQuantity(id, quanitity), 
    //                                             HttpStatus.OK);
    // }


    // @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    // public ResponseEntity<TradeRequest> update(@PathVariable String id, @RequestBody TradeRequest tradeRequest) {
    //     LOG.debug("update() request received: " + id);

    //     return new ResponseEntity<TradeRequest>(tradeService.update(id, tradeRequest), HttpStatus.OK);
    // }  

    
    @RequestMapping(path="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable String id) {
        LOG.debug("delete() request received: " + id);
        
        tradeService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
    
}
