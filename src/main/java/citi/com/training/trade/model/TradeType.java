package citi.com.training.trade.model;

public enum TradeType {
    BUY, SELL;
}
