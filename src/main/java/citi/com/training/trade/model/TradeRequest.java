package citi.com.training.trade.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;

/**
 * Class to represent a TradeRequest
 */
public class TradeRequest {

	private String id;
	private LocalDateTime dateTimeCreated;
	private String stockTicker;
	private int stockQuantity;
	private double requestedPrice;
	private TradeType tradeType;
	private TradeState tradeState;

	/**
	 * Zero argument constructor
	 */
	public TradeRequest() {
		this.setDateTimeCreated(this.initialiseTradeDateTime());
		this.setTradeState(TradeState.CREATED);
	}

	/**
	 * Constructor with arguments
	 * @param dateTimeCreated
	 * @param stockTicker
	 * @param stockQuantity
	 * @param requestedPrice
	 * @param tradeType
	 */
	public TradeRequest(String stockTicker, int stockQuantity, double requestedPrice, TradeType tradeType) {
		this.initialiseTradeDateTime();
		this.setStockTicker(stockTicker);
		this.setStockQuantity(stockQuantity);
		this.setRequestedPrice(requestedPrice);
		this.setTradeType(tradeType);
		this.setTradeState(TradeState.CREATED);
	}

	/**
	 * Getter for Id
	 * 
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * Setter for Id
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Getter for datetime created
	 * 
	 * @return
	 */
	public LocalDateTime getDateTimeCreated() {
		return dateTimeCreated;
	}

	/**
	 * Setter for datetime created
	 * 
	 * @param dateTimeCreated
	 */
	public void setDateTimeCreated(LocalDateTime dateTimeCreated) {
		this.dateTimeCreated = dateTimeCreated;
	}

	/**
	 * Getter for stock ticker
	 * 
	 * @return stockTicker
	 */
	public String getStockTicker() {
		return stockTicker;
	}

	/**
	 * Setter for stock ticker
	 * 
	 * @param stockTicker
	 */
	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	/**
	 * Getter for stock quantity
	 * 
	 * @return stockQuantity
	 */
	public int getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Setter for stock quantity
	 * 
	 * @param stockQuantity
	 */
	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	/**
	 * Getter for requested price
	 * 
	 * @return
	 */
	public double getRequestedPrice() {
		return requestedPrice;
	}

	/**
	 * Setter for requested price
	 * 
	 * @param requestedPrice
	 */
	public void setRequestedPrice(double requestedPrice) {
		this.requestedPrice = requestedPrice;
	}

	/**
	 * Getter for trade type
	 * @return tradeType
	 */
	public TradeType getTradeType() {
		return tradeType;
	}

	/**
	 * Setter for trade type
	 * @param tradeType
	 */
	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}

	/**
	 * Getter for trade state
	 * @return
	 */
	public TradeState getTradeState() {
		return tradeState;
	}

	/**
	 * Setter for trade state
	 * @param tradeState
	 */
	public void setTradeState(TradeState tradeState) {
		this.tradeState = tradeState;
	}

	/**
	 * Gets local datetime, formats it and returns it 
	 * @return
	 */
	private LocalDateTime initialiseTradeDateTime() {
		LocalDateTime dateTimeCreated = LocalDateTime.now();
        /* DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		dateTimeCreated.format(formatter); */
		return LocalDateTime.now();
	}

	/**
	 * Compare two TradeRequest objects
	 * @param tradeRequest
	 * @return
	 */
	public boolean equals(TradeRequest tradeRequest) {
		return ((this.getId().equals(tradeRequest.getId())) &&
				(this.getDateTimeCreated().equals(tradeRequest.getDateTimeCreated())) &&
				(this.getStockTicker().equals(tradeRequest.getStockTicker())) &&
				(this.getStockQuantity() == tradeRequest.getStockQuantity()) &&
				(this.getRequestedPrice() == tradeRequest.getRequestedPrice()) &&
				(this.getTradeType().equals(tradeRequest.getTradeType())) && 
				(this.getTradeState().equals(tradeRequest.getTradeState())));
	}

	/**
	 * Returns representation of trade request
	 */
	@Override
	public String toString() {
		return "TradeRequest [dateTimeCreated=" + dateTimeCreated + ", id=" + id + ", requestedPrice=" + requestedPrice
				+ ", stockQuantity=" + stockQuantity + ", stockTicker=" + stockTicker + ", tradeState=" + tradeState
				+ ", tradeType=" + tradeType + "]";
	}

	

}
