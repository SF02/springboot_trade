package citi.com.training.trade.model;

public enum TradeState {
    CREATED, PENDING, CANCELLED, REJECTED, FILLED, PARTIALLY_FILLED, ERROR;
}
