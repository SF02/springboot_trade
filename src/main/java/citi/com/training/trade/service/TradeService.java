package citi.com.training.trade.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import citi.com.training.trade.dao.TradeMongoRepo;
import citi.com.training.trade.model.TradeRequest;
import citi.com.training.trade.model.TradeState;

@Component
public class TradeService {

    @Autowired
    private TradeMongoRepo mongoRepo;


    public List<TradeRequest> findAll() {
        return mongoRepo.findAll();
    }

    // doesnt run when this isnt commented out
    public Optional<TradeRequest> findOne(String id) {
            //return mongoRepo.findOneById(id);
            return mongoRepo.findById(id);
    }


    public TradeRequest save(TradeRequest tradeRequest) {
        return mongoRepo.save(tradeRequest);
    }


    // public TradeRequest update(String id, TradeRequest updateTradeRequest) {

    //     TradeRequest originalTradeRequest = findOne(id);
    //     if (originalTradeRequest.getTradeState().equals(TradeState.CREATED)) {
    //         updateTradeRequest.setId(id);
    //         return mongoRepo.save(tradeRequest);
    //     }
    //         return null;
    // }

    /////V2
    // public TradeRequest updateQuantity(String id, int quantity){
        
    //     TradeRequest updatingTradeRequest = mongoRepo.findOneById(id);
    //     updatingTradeRequest.setStockQuantity(quantity);
    //     return updatingTradeRequest;
    // }


    

    public void delete(String id) {
        mongoRepo.deleteById(id);
    }
    
}
