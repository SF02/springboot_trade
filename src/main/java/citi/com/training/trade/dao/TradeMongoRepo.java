package citi.com.training.trade.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import citi.com.training.trade.model.TradeRequest;

@Component
public interface TradeMongoRepo extends MongoRepository<TradeRequest, String>{
    
    public TradeRequest findOneById(String Id);
}